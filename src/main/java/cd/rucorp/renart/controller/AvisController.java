/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.controller;

import cd.rucorp.renart.entities.Jaime;
import cd.rucorp.renart.forms.JaimeForm;
import cd.rucorp.renart.services.AvisService;
import cd.rucorp.renart.utilities.HttpDataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rubenpanga
 */

@RestController
@CrossOrigin
@RequestMapping("avis")
public class AvisController {
    @Autowired
    private AvisService service;
    
    @PostMapping("/like-oeuvre")
    public HttpDataResponse<Jaime> likeOeuvre(@RequestBody JaimeForm jaime){
        return service.likeOeuvre(jaime);
    }
}
