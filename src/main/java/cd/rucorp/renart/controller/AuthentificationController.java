/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.controller;

import cd.rucorp.renart.entities.User;
import cd.rucorp.renart.services.AuthentificationService;
import cd.rucorp.renart.utilities.AppConst;
import cd.rucorp.renart.utilities.HttpDataResponse;
import cd.rucorp.renart.utilities.ValueDataException;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rubenpanga
 */

@RestController
@CrossOrigin
public class AuthentificationController {
    @Autowired
    private AuthentificationService service;
    
    @PostMapping("connexion")
    public HttpDataResponse<User> connexion(@RequestBody ObjectNode data){
        HttpDataResponse<User> httpResponse = new HttpDataResponse<>();
        try{
            String username = null, password = null;
            
            if(data == null)
                throw new ValueDataException(AppConst.INCORRECT_PARAMS);
            
            if(data.has("username"))
                username = data.get("username").asText();
            if(data.has("password"))
                password = data.get("password").asText();
            
            httpResponse = service.connexion(username, password);
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    @PostMapping("signup")
    public HttpDataResponse<User> register(@RequestBody User user, Errors errors){
        return service.signUp(user, errors);
    } 
    
    @PostMapping("update-password")
    public HttpDataResponse<Boolean> updatePassword(@RequestBody ObjectNode data){
        HttpDataResponse<Boolean> httpResponse = new HttpDataResponse<>();
        try{
            String newPassword = null, confirmPassword = null, oldPassword = null;
            Long id = null;
            
            if(data == null)
                throw new ValueDataException(AppConst.INCORRECT_PARAMS);
            
            if(data.has("newPassword"))
                newPassword = data.get("newPassword").asText();
            if(data.has("confirmPassword"))
                confirmPassword = data.get("confirmPassword").asText();
            if(data.has("oldPassword"))
                oldPassword = data.get("oldPassword").asText();
            if(data.has("id"))
                id = data.get("id").asLong();
            
            httpResponse = service.updatePassword(newPassword, confirmPassword, oldPassword, id);
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
}
