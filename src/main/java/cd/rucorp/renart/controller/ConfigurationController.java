/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.controller;

import cd.rucorp.renart.entities.Artiste;
import cd.rucorp.renart.entities.CategorieOeuvre;
import cd.rucorp.renart.entities.Oeuvre;
import cd.rucorp.renart.entities.OeuvrePhoto;
import cd.rucorp.renart.repositories.OeuvreRepository;
import cd.rucorp.renart.services.ConfigurationService;
import cd.rucorp.renart.utilities.HttpDataResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import javax.validation.Valid;
import jdk.jfr.BooleanFlag;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rubenpanga
 */

@RestController
@RequestMapping("config")
@CrossOrigin
public class ConfigurationController {
    @Autowired
    private ConfigurationService service;
    
    @Autowired
    private OeuvreRepository repo;
    
    @PostMapping("/save-oeuvre")
    public HttpDataResponse<Oeuvre> saveOeuvre(@Valid @RequestBody Oeuvre oeuvre, Errors erros){
        return service.saveOeuvre(oeuvre, erros);
    }
    
    @PostMapping("/save-artiste")
    public HttpDataResponse<Artiste> saveArtiste(@Valid @RequestBody Artiste artiste, Errors erros){
        return service.saveArtiste(artiste, erros);
    }
    @GetMapping("/get-liste-artistes")
    public HttpDataResponse<List<Artiste>> getListArtiste(){
        return service.getListArtiste();
    }
    @GetMapping("/get-liste-oeuvres")
    public HttpDataResponse<List<Oeuvre>> getListOeuvre(){
        return service.getListOeuvre();
    }
    
    @GetMapping("/get-liste-categories-oeuvre")
    public HttpDataResponse<List<CategorieOeuvre>> getListCategorieOeuvre(){
        return service.getListCategorie();
    }
    
    @GetMapping("/oeuvre-photo/{id}")
    public ResponseEntity<Resource> getOeuvreImage(@PathVariable Long id) {
//        HttpDataResponse<OeuvrePhoto> httpResponse = service.getOeuvrePhoto(id);

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + id + ".png");
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        HttpStatus status = HttpStatus.NOT_FOUND;
        InputStreamResource resource = null;

        File file = null;

        try {
            OeuvrePhoto fingerprint = service.getOeuvrePhoto(id).getResponse();
            if (fingerprint == null) {
                throw new Exception();
            }

            file = new File(fingerprint.getPhotoUrl());

            if (!file.exists()) {
                throw new Exception();
            }
            
//            resource = new InputStreamResource(new FileInputStream(file));
            resource = new InputStreamResource(new FileInputStream(file));

            status = HttpStatus.OK;

        } catch (Exception e) {
//            e.printStackTrace();
        } finally {
            return ResponseEntity
                    .status(status)
                    .contentLength(file == null ? 0 : file.length())
                    .headers(header)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        }
    }
    
    @GetMapping("/artiste-profil/{id}")
    public ResponseEntity<Resource> getArtisteProfil(@PathVariable Long id) {
//        HttpDataResponse<OeuvrePhoto> httpResponse = service.getOeuvrePhoto(id);

        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + id + ".png");
        header.add("Cache-Control", "no-cache, no-store, must-revalidate");
        header.add("Pragma", "no-cache");
        header.add("Expires", "0");

        HttpStatus status = HttpStatus.NOT_FOUND;
        InputStreamResource resource = null;

        File file = null;

        try {
            Artiste fingerprint = service.getArtiste(id).getResponse();
            if (fingerprint == null) {
                throw new Exception();
            }

            file = new File(fingerprint.getProfil());

            if (!file.exists()) {
                throw new Exception();
            }
            
//            resource = new InputStreamResource(new FileInputStream(file));
            resource = new InputStreamResource(new FileInputStream(file));

            status = HttpStatus.OK;

        } catch (Exception e) {
//            e.printStackTrace();
        } finally {
            return ResponseEntity
                    .status(status)
                    .contentLength(file == null ? 0 : file.length())
                    .headers(header)
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .body(resource);
        }
    }
    
    @GetMapping("artiste/{id}")
    public HttpDataResponse<Artiste> getArtiste(@PathVariable Long id){
        return service.getArtiste(id);
    }
    
    @GetMapping("oeuvre/{id}")
    public HttpDataResponse<Oeuvre> getOeuvre(@PathVariable Long id){
        return service.getOeuvre(id);
    }
    
    @GetMapping("oeuvre/find-by-artiste/{id}")
    public HttpDataResponse<List<Oeuvre>> getOeuvreByArtiste(@PathVariable Long id){
        return service.getOeuvreByArtiste(id);
    }
    
    @GetMapping("artiste/find/{keyword}")
    public HttpDataResponse<List<Artiste>> findByKeyword(@PathVariable String keyword){
        return service.findByKeyWord(keyword);
    }
    
    
    
}
