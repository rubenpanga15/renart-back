/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.adapters;

import cd.rucorp.renart.entities.Commentaire;
import cd.rucorp.renart.entities.Jaime;
import cd.rucorp.renart.entities.Oeuvre;
import cd.rucorp.renart.entities.OeuvrePhoto;
import java.util.List;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

/**
 *
 * @author rubenpanga
 */
public class OeuvreAdapter extends Oeuvre {
    @OneToMany(mappedBy = "oeuvre", fetch = FetchType.LAZY)
    private List<OeuvrePhoto> oeuvrePhotos;
    @OneToMany(mappedBy = "oeuvre", fetch = FetchType.LAZY)
    private List<Commentaire> commentaires;
    @OneToMany(mappedBy = "oeuvre", fetch = FetchType.LAZY)
    private List<Jaime> jaimes;
}
