
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rubenpanga
 */
@Data
@NoArgsConstructor
@Entity
//@JsonIdentityInfo(
//  generator = ObjectIdGenerators.PropertyGenerator.class, 
//  property = "id")
public class Artiste implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Veuillez renseigner le nom")
    private String nom;
    @NotBlank(message = "Veuillez renseigner le postnom")
    private String postnom;
    @NotBlank(message = "Veuillez renseigner le prénom")
    private String prenom;
    @NotBlank(message = "Veuillez renseigner le lieu de naissance")
    private String lieuNaissance;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateNaissance;
    @Pattern(message = "Veuillez renseigner un numéro de téléphone valide", regexp = "^\\(?([0-9]{3})\\)?[-.\\s]?([0-9]{3})[-.\\s]?([0-9]{4})$")
    private String telephone;
    @Pattern(regexp = "^(.+)@(.+)$", message = "Veuillez renseigner une adresse mail valide")
    private String email;
//    @NotBlank(message = "Veuillez renseigner l'adresse")
//    private String adresse;
    @NotBlank(message = "Veuillez renseigner la biographie")
    private String biographie;
    private String nationalite;
    private String profil;
    private boolean status;
    private Date dateCreat;
    
    @Transient
    private String profilData;
    @Transient
    private String profilExtension;
    
//    @OneToMany(mappedBy = "artiste", fetch = FetchType.LAZY)
////    @JsonManagedReference
//    List<Oeuvre> oeuvres;

}
