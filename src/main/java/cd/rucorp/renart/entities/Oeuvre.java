/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rubenpanga
 */

@Data
@NoArgsConstructor
@Entity
//@JsonIdentityInfo(
//  generator = ObjectIdGenerators.PropertyGenerator.class, 
//  property = "id")
public class Oeuvre implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;
    @NotNull
    @NotBlank(message = "Veuillez renseigner le nom")
    protected String nom;
//    @NotNull
//    @NotBlank(message = "Veuillez renseigner le code")
//    @Column(unique = true)
//    private String code;
    @NotNull
    @NotBlank(message = "Veuillez renseigner l'origine de l'oeuvre")
    protected String origine;
    @ManyToOne
    @NotNull(message = "Veuillez renseigner l'artiste")
//    @JsonBackReference
    protected Artiste artiste;
    @ManyToOne
    @NotNull(message = "Veuillez renseigner la categorie")
    protected CategorieOeuvre categorieOeuvre;
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd")
    protected Date dateCreation;
    @Column(columnDefinition = "text not null")
    @NotBlank(message = "Veuillez renseigner la description")
    protected String description;
    @NotNull(message = "Veuillez renseigner le prix")
    protected Double prix;
    protected Boolean status;
    protected Date dateCreat;
    
    @OneToMany(mappedBy = "oeuvre")
    @JsonManagedReference
    private List<OeuvrePhoto> oeuvrePhotos;
    @OneToMany(mappedBy = "oeuvre")
    @JsonManagedReference
    private List<Commentaire> commentaires;
    @JsonManagedReference
    @OneToMany(mappedBy = "oeuvre", fetch = FetchType.LAZY)
    private List<Jaime> jaimes;
}
