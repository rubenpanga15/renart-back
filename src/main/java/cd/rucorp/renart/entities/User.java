/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author rubenpanga
 */

@Data
@NoArgsConstructor
@Entity
public class User implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    @NotBlank(message = "Veuillez renseigner le nom")
    private String noms;
    @NotNull
    @NotBlank(message = "Veuillez renseigner le nom d'utilisateur")
    private String username;
    @Pattern(regexp = "^(.+)@(.+)$", message = "Veuillez renseigner une adresse mail valide")
    private String email;
    @NotNull
    @Pattern(message = "Veuillez renseigner un numéro de téléphone valide", regexp = "^\\(?([0-9]{3})\\)?[-.\\s]?([0-9]{3})[-.\\s]?([0-9]{4})$")
    private String telephone;
    private String password;
    private String role;
    private boolean status;
    private Date dateCreat;
    
}
