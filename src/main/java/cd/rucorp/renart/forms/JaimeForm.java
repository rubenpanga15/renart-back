/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.forms;

import cd.rucorp.renart.entities.Jaime;
import lombok.Data;

/**
 *
 * @author rubenpanga
 */
@Data
public class JaimeForm {
    private Jaime jaime;
}
