/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.repositories;

import cd.rucorp.renart.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author rubenpanga
 */
public interface UserRepository extends JpaRepository<User, Long> {
    public User findByUsernameAndPassword(String username, String password);
}
