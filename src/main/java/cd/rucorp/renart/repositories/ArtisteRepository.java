/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.repositories;

import cd.rucorp.renart.entities.Artiste;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author rubenpanga
 */
public interface ArtisteRepository extends JpaRepository<Artiste, Long> {
    @Query("select a from Artiste a where a.nom like %:keyword% or a.postnom like %:keyword% or a.prenom like %:keyword%")
    List<Artiste> findByKeyWord(String keyword);
    
    Artiste findArtisteById(Long id);
}
