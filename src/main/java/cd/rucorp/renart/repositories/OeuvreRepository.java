/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.repositories;

import cd.rucorp.renart.entities.Artiste;
import cd.rucorp.renart.entities.Oeuvre;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rubenpanga
 */
@Repository
public interface OeuvreRepository extends JpaRepository<Oeuvre, Long> {
    List<Oeuvre> findByArtiste(Artiste artiste);
}
