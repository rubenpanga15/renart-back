/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.repositories;

import cd.rucorp.renart.entities.OeuvrePhoto;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author rubenpanga
 */
public interface OeuvrePhotoRepository extends JpaRepository<OeuvrePhoto, Long> {
    
}
