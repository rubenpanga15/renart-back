/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.repositories;

import cd.rucorp.renart.entities.Commentaire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author rubenpanga
 */
@Repository
public interface CommentaireRepository extends JpaRepository<Commentaire, Long> {
    
}
