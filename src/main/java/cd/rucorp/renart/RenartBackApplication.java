package cd.rucorp.renart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RenartBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(RenartBackApplication.class, args);
	}

}
