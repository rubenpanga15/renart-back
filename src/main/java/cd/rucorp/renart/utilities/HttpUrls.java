/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.utilities;

/**
 *
 * @author rubenpanga
 */
public class HttpUrls {
    private static final String SCHEMA = "http://";

    //ruben
    //private static final String IP = "172.16.20.18";

    //Test Coco
    private static final String IP = "102.68.62.41";
    private static final  String PORT = "3000";
    private static final String CONTEXT = "census/";

    private static final String getBase(){
        return SCHEMA + IP + ":" + PORT + "/" + CONTEXT;
    }
    
    public static final String BASE_URL_PHOTO = getBase() + "api/get-photo";
}
