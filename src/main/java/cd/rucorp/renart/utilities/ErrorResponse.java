/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.utilities;

import lombok.Data;

/**
 *
 * @author rubenpanga
 */
@Data
public class ErrorResponse {
    private String errorCode;
    private String errorDescription;
    
    public ErrorResponse(){
        this.errorCode = "KO";
    }
}
