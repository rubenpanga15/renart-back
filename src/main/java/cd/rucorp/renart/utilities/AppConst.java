/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.utilities;

/**
 *
 * @author rubenpanga
 */
public class AppConst {
    public static final String OPERATION_FAILED = "L'opération n'a pas abouti. Veuillez réeassayer ou contacter le support technique";
    public static final String UNKNOWN_ERROR = "Une erreur inconnue s'est produite. Veuillez réeassayer ou contacter le support technique";
    public static final String INCORRECT_PARAMS = "Les paramètres envoyés sont incorrects.";
    public static final String EMPTY_PARAMS = "Les paramètres sont vides";
    
//     public static final String BASE_PATH_PHOTO_OEUVRE = "/Users/rubenpanga/Documents/photos/renart/";
//     public static final String BASE_PATH_PHOTO_ARTISTE = "/Users/rubenpanga/Documents/photos/artiste/";
     
     public static final String BASE_PATH_PHOTO_OEUVRE = "/home/renart/data/oeuvres/";
     public static final String BASE_PATH_PHOTO_ARTISTE = "/home/renart/data/profil/";
    
//    public static final String BASE_PATH_PHOTO = "/opt/wildfly/pics";
//    public static final String BASE_PATH_WSQ = "/Users/rubenpanga/Documents/photos/wsq/";
}
