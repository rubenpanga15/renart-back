/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.utilities;

/**
 *
 * @author rubenpanga
 */
public class ValueDataException extends Exception {

    public ValueDataException(String message) {
        super(message);
    }
}
