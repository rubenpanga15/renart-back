/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.utilities;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import javax.imageio.ImageIO;

/**
 *
 * @author rubenpanga
 */
public class AppUtilities {
    public static void controleValue(String value, String message) throws ValueDataException {
        if(value == null || value.trim().isEmpty())
            throw new ValueDataException(message);
    }
    
    public static void controleValue(Integer value, String message) throws ValueDataException {
        if(value == null || value <= 0)
            throw new ValueDataException(message);
    }
    
    public static void controleValue(Long value, String message) throws ValueDataException {
        if(value == null || value <= 0)
            throw new ValueDataException(message);
    }
    
    public static void controleValue(Float value, String message) throws ValueDataException {
        if(value == null || value <= 0)
            throw new ValueDataException(message);
    }
    
    public static void controleValue(Double value, String message) throws ValueDataException {
        if(value == null || value <= 0)
            throw new ValueDataException(message);
    }
    
    public static BufferedImage decodeStringToImage(String data){
        BufferedImage image = null;
        byte[] imageByte = null;
        
        try{
            data = getDataString(data);
//            imageByte = Base64.decodeBase64(data);
//            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
//            image = ImageIO.read(bis);
//            bis.close();
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return image;
    }
    
    private static String getDataString(String str){
        String data = str;
        
        if(data.startsWith("data:image"))
            data = data.substring(data.indexOf(",") + 1);
        
        return data;
    }
    
    public static byte[] decodeAndroidBase64ToByteArray(String datab64){
        byte[] data = null;
        try{
//            data = android.util.Base64.decode(datab64, android.util.Base64.DEFAULT);
            
//            data = Base64.decodeBase64(datab64);

        }catch(Exception e){
            e.printStackTrace();
        }
        
        return data;
    }
    
    public static boolean saveFile(BufferedImage image, String filePath, String extension){
        boolean done = false;
        File file = new File(filePath);
        
        try{
            done = ImageIO.write(image, extension, file);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        return done;
    }
    
    
}
