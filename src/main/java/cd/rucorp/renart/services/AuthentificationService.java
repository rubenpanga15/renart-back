/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.services;

import cd.rucorp.renart.entities.User;
import cd.rucorp.renart.repositories.UserRepository;
import cd.rucorp.renart.utilities.AppConst;
import cd.rucorp.renart.utilities.AppUtilities;
import cd.rucorp.renart.utilities.HttpDataResponse;
import cd.rucorp.renart.utilities.ValueDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

/**
 *
 * @author rubenpanga
 */

@Service
public class AuthentificationService {
    @Autowired
    private UserRepository repository;
    
    public HttpDataResponse<User> connexion(String username, String password){
        HttpDataResponse<User> httpResponse = new HttpDataResponse<User>();
        
        try{
            AppUtilities.controleValue(username, "Veuillez le nom d'utilisateur");
            AppUtilities.controleValue(password, "Veuillez renseigner le mot de passe");
            
            User user = repository.findByUsernameAndPassword(username, password);
            
            if(user == null)
                throw new ValueDataException("Nom d'utilisateur ou mot de passe incorrect");
            
            httpResponse.getError().setErrorCode("OK");
            httpResponse.setResponse(user);
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<User> signUp(User user, Errors errors){
        HttpDataResponse<User> httpResponse = new HttpDataResponse<User>();
        
        try{
            if(errors.hasErrors())
                throw new ValueDataException(errors.getAllErrors().get(0).getDefaultMessage());
            AppUtilities.controleValue(user.getNoms(), "Veuillez renseigner votre nom au complet");
//            AppUtilities.controleValue(user.getRole(), "Veuillez renseigner le role");
            AppUtilities.controleValue(user.getTelephone(), "Veuillez renseigner le numéro de téléphone");
            
            user.setRole("CLIENT");
            user.setStatus(true);
            
            user = repository.save(user);
            
            if(user == null)
                throw new ValueDataException("Nom d'utilisateur ou mot de passe incorrect");
            
            httpResponse.getError().setErrorCode("OK");
            httpResponse.setResponse(user);
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<Boolean> updatePassword(String newPassword,String confirmPassword,String oldPassword, Long id){
        HttpDataResponse<Boolean> httpResponse = new HttpDataResponse<>();
        
        try{
            AppUtilities.controleValue(newPassword, "Veuillez renseigner le nouveau mot de  passe");
            AppUtilities.controleValue(oldPassword, "Veuillez renseigner l'ancien mot de passe");
            AppUtilities.controleValue(id, "Veuillez rensienger l'utilisateur");
            
            User user = repository.getById(id);
            
            if(user == null)
                throw new ValueDataException("cet utilisateur n'existe pas dans le système");
            
            if(!user.getPassword().equals(oldPassword))
                throw new ValueDataException("L'ancien mot de passe est incorrect");
            
            if(!newPassword.equals(confirmPassword))
                throw new ValueDataException("Les mots de passe ne correspondent pas");
            
            user.setPassword(newPassword);
            
            user = repository.save(user);
            
            if(user == null)
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            
            httpResponse.getError().setErrorCode("OK");
            httpResponse.getError().setErrorDescription("");
            httpResponse.setResponse(true);
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
}
