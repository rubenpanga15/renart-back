/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.services;

import cd.rucorp.renart.entities.Artiste;
import cd.rucorp.renart.entities.CategorieOeuvre;
import cd.rucorp.renart.entities.Oeuvre;
import cd.rucorp.renart.entities.OeuvrePhoto;
import cd.rucorp.renart.repositories.ArtisteRepository;
import cd.rucorp.renart.repositories.CategorieOeuvreRepository;
import cd.rucorp.renart.repositories.OeuvrePhotoRepository;
import cd.rucorp.renart.repositories.OeuvreRepository;
import cd.rucorp.renart.utilities.AppConst;
import cd.rucorp.renart.utilities.AppUtilities;
import cd.rucorp.renart.utilities.HttpDataResponse;
import cd.rucorp.renart.utilities.Logger;
import cd.rucorp.renart.utilities.ValueDataException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

/**
 *
 * @author rubenpanga
 */

@Service
public class ConfigurationService {
    @Autowired
    private OeuvreRepository oeuvreRepository;
    @Autowired
    private OeuvrePhotoRepository oeuvrePhotoRepository;
    @Autowired
    private ArtisteRepository artisteRepository;
    
    @Autowired
    private CategorieOeuvreRepository categorieOeuvreRepository;
    
    public HttpDataResponse<Oeuvre> saveOeuvre(Oeuvre oeuvre, Errors errors){
        HttpDataResponse<Oeuvre> httpResponse = new HttpDataResponse<>();
        
        try{
            if(errors.hasErrors())
                throw new ValueDataException(errors.getAllErrors().get(0).getDefaultMessage());
            
            oeuvre.setStatus(true);
            oeuvre.setDateCreat(new Date());
           
            oeuvre = oeuvreRepository.save(oeuvre);
            
            if(oeuvre == null)
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            
            List<OeuvrePhoto> photos = oeuvre.getOeuvrePhotos();
            
            for(int i = 0; i < photos.size(); i++){
                OeuvrePhoto photo = photos.get(i);
                photos.get(i).setOeuvre(oeuvre);
                
                photo = savePhoto(photos.get(i)).getResponse();
                if(photo != null)
                    photos.get(i).setId(photo.getId());
            }
            
            httpResponse.setResponse(oeuvre);
            httpResponse.getError().setErrorCode("OK");
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<OeuvrePhoto> savePhoto(OeuvrePhoto oeuvrePhoto){
        HttpDataResponse<OeuvrePhoto> httpResponse = new HttpDataResponse<>();
        
        try{
            AppUtilities.controleValue(oeuvrePhoto.getData(), "Veuillez renseigner le data");
            AppUtilities.controleValue(oeuvrePhoto.getExtension(), "Veuillez renseigner l'extension");
            
            oeuvrePhoto = oeuvrePhotoRepository.save(oeuvrePhoto);
            
            if(oeuvrePhoto == null)
                throw new ValueDataException("Le photo n'a pas été enregistrée");
            
            BufferedImage buffImage = null;
            byte[] imageByte = null;
            
            imageByte = Base64.decodeBase64(oeuvrePhoto.getData());
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            
            buffImage = ImageIO.read(bis);
            bis.close();
            
            String fileName = AppConst.BASE_PATH_PHOTO_OEUVRE + "" + oeuvrePhoto.getId() + "." + oeuvrePhoto.getExtension();
            
            File fileOuput = new File(fileName);
            
            if(!ImageIO.write(buffImage, oeuvrePhoto.getExtension(), fileOuput))
                throw new ValueDataException("Image not enregistrée");
            
            oeuvrePhoto.setPhotoUrl(fileName);
            
            oeuvrePhoto = oeuvrePhotoRepository.save(oeuvrePhoto);
            
            if(oeuvrePhoto == null)
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            
            httpResponse.getError().setErrorCode("OK");
            httpResponse.setResponse(oeuvrePhoto);
        }catch(ValueDataException e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<Artiste> savePhoto(Artiste artiste){
        HttpDataResponse<Artiste> httpResponse = new HttpDataResponse<>();
        
        try{
            AppUtilities.controleValue(artiste.getProfilData(), "Veuillez renseigner le data");
            AppUtilities.controleValue(artiste.getProfilExtension(), "Veuillez renseigner l'extension");
            
//            artiste = artisteRepository.save(artiste);
            
            if(artiste == null)
                throw new ValueDataException("Le photo n'a pas été enregistrée");
            
            BufferedImage buffImage = null;
            byte[] imageByte = null;
            
            imageByte = Base64.decodeBase64(artiste.getProfilData());
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            
            buffImage = ImageIO.read(bis);
            bis.close();
            
            String fileName = AppConst.BASE_PATH_PHOTO_ARTISTE + "" + artiste.getId() + "." + artiste.getProfilExtension();
            
            File fileOuput = new File(fileName);
            
            if(!ImageIO.write(buffImage, artiste.getProfilExtension(), fileOuput))
                throw new ValueDataException("Image not enregistrée");
            
            artiste.setProfil(fileName);
            
            artiste = artisteRepository.save(artiste);
            
            if(artiste == null)
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            
            httpResponse.getError().setErrorCode("OK");
            httpResponse.setResponse(artiste);
        }catch(ValueDataException e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<Artiste> saveArtiste(Artiste artiste, Errors errors){
        HttpDataResponse<Artiste> httpResponse = new HttpDataResponse<>();
        
        try{
            
            if(errors.hasErrors())
                throw new ValueDataException(errors.getAllErrors().get(0).getDefaultMessage());
            
            artiste = artisteRepository.save(artiste);
            
            if(artiste == null)
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            
            savePhoto(artiste);
            
            httpResponse.setResponse(artiste);
            httpResponse.getError().setErrorCode("OK");
            
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
        public HttpDataResponse<List<Oeuvre>> getListOeuvre(){
        HttpDataResponse<List<Oeuvre>> httpResponse = new HttpDataResponse<>();
        
        try{
            List<Oeuvre> oeuvres = oeuvreRepository.findAll();
            
            if(oeuvres == null || oeuvres.size() <= 0)
                throw new ValueDataException("Aucune oeuvre trouvée");
            
            httpResponse.setResponse(oeuvres);
            httpResponse.getError().setErrorCode("OK");
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<List<Artiste>> getListArtiste(){
        HttpDataResponse<List<Artiste>> httpResponse = new HttpDataResponse<>();
        
        try{
            List<Artiste> artistes = artisteRepository.findAll();
            
            if(artistes == null || artistes.size() <= 0)
                throw new ValueDataException("Aucun artiste trouvée");
            
            httpResponse.setResponse(artistes);
            httpResponse.getError().setErrorCode("OK");
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<List<CategorieOeuvre>> getListCategorie(){
        HttpDataResponse<List<CategorieOeuvre>> httpResponse = new HttpDataResponse<>();
        
        try{
            List<CategorieOeuvre> artistes = categorieOeuvreRepository.findAll();
            
            if(artistes == null || artistes.size() <= 0)
                throw new ValueDataException("Aucune categorie trouvée");
            
            httpResponse.setResponse(artistes);
            httpResponse.getError().setErrorCode("OK");
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<OeuvrePhoto> getOeuvrePhoto(Long id){
        HttpDataResponse<OeuvrePhoto> httpResponse = new HttpDataResponse<>();
        
        try{
            AppUtilities.controleValue(id, "Veuillez renseigner l'id");
            
            OeuvrePhoto photo = oeuvrePhotoRepository.getById(id);
            
            if(photo == null)
                throw new ValueDataException("Aucune photo trouvée");
            
            httpResponse.setResponse(photo);
            httpResponse.getError().setErrorCode("OK");
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<Artiste> getArtiste(Long id){
        HttpDataResponse<Artiste> httpResponse = new HttpDataResponse<>();
        
        try{
            AppUtilities.controleValue(id, "Veuillez renseigner l'id");
            
            Artiste photo = artisteRepository.findArtisteById(id);
            
            if(photo == null)
                throw new ValueDataException("Aucune photo trouvée");
            
            httpResponse.setResponse(photo);
            httpResponse.getError().setErrorCode("OK");
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<Oeuvre> getOeuvre(Long id){
        HttpDataResponse<Oeuvre> httpResponse = new HttpDataResponse<>();
        
        try{
            AppUtilities.controleValue(id, "Veuillez renseigner l'id");
            
            Optional<Oeuvre> photo = oeuvreRepository.findById(id);
            
            if(photo == null || !photo.isPresent())
                throw new ValueDataException("Aucune photo trouvée");
            
            httpResponse.setResponse(photo.get());
            httpResponse.getError().setErrorCode("OK");
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<List<Artiste>> findByKeyWord(String keyword){
        HttpDataResponse<List<Artiste>> httpResponse = new HttpDataResponse<List<Artiste>>();
        
        try{
            AppUtilities.controleValue(keyword, "Veuillez renseigner le mot clé");
            
            List<Artiste> dataset = artisteRepository.findByKeyWord(keyword);
            
            if(dataset == null || dataset.size() <= 0)
                throw new ValueDataException("Aucun artiste trouvé");
            
            httpResponse.getError().setErrorCode("OK");
            httpResponse.getError().setErrorDescription("");
            httpResponse.setResponse(dataset);
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    public HttpDataResponse<List<Oeuvre>> getOeuvreByArtiste(Long id){
        HttpDataResponse<List<Oeuvre>> httpResponse = new HttpDataResponse<>();
        
        try{
            
            AppUtilities.controleValue(id, "Veuillez renseigner l'id");
            
            Artiste artiste = artisteRepository.findArtisteById(id);
            
            if(artiste == null)
                throw new ValueDataException("Cet artiste n'est pas enregistré dans le système");
            
            List<Oeuvre> oeuvres = oeuvreRepository.findByArtiste(artiste);
            
            if(oeuvres == null || oeuvres.size() <= 0)
                throw new ValueDataException("Aucune oeuvre trouvée");
            
            httpResponse.setResponse(oeuvres);
            httpResponse.getError().setErrorCode("OK");
            
        }catch(ValueDataException e){
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
    
    
}
