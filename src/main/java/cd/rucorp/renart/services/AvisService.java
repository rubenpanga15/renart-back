/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cd.rucorp.renart.services;

import cd.rucorp.renart.entities.Jaime;
import cd.rucorp.renart.forms.JaimeForm;
import cd.rucorp.renart.repositories.CommentaireRepository;
import cd.rucorp.renart.repositories.JaimeRepository;
import cd.rucorp.renart.repositories.OeuvreRepository;
import cd.rucorp.renart.utilities.AppConst;
import cd.rucorp.renart.utilities.HttpDataResponse;
import cd.rucorp.renart.utilities.ValueDataException;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author rubenpanga
 */

@Service
public class AvisService {
    @Autowired
    private JaimeRepository jaimeRepository;
    @Autowired
    private CommentaireRepository commentaireRepository;
    @Autowired
    private OeuvreRepository oeuvreRepository;
    
    public HttpDataResponse<Jaime> likeOeuvre(JaimeForm form){
        HttpDataResponse<Jaime> httpResponse = new HttpDataResponse<>();
        
        try{
            if(form == null)
                throw new ValueDataException(AppConst.INCORRECT_PARAMS);
            
            Jaime jaime = form.getJaime();
            
            jaime.setStatus(true);
            jaime.setDateCreat(new Date());
            
            jaime = jaimeRepository.save(jaime);
            
            if(jaime == null)
                throw new ValueDataException(AppConst.OPERATION_FAILED);
            
            httpResponse.getError().setErrorCode("OK");
            httpResponse.setResponse(jaime);
            
        }catch(ValueDataException e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(e.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            httpResponse.getError().setErrorCode("KO");
            httpResponse.getError().setErrorDescription(AppConst.UNKNOWN_ERROR);
        }
        
        return httpResponse;
    }
}
